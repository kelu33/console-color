interface Tag {
    regex: RegExp,
    result?: string,
    resultWithArgs?(...args: string[]): string
}

const tags: Tag[] = [
    // actions
    { regex: /\$<reset>/, result: '\x1b[0m' },
    { regex: /\$<bright>/, result: '\x1b[1m' },
    { regex: /\$<dim>/, result: '\x1b[2m' },
    { regex: /\$<underscore>/, result: '\x1b[4m' },
    { regex: /\$<blink>/, result: '\x1b[5m' },
    { regex: /\$<reverse>/, result: '\x1b[7m' },
    { regex: /\$<hidden>/, result: '\x1b[8m' },
    // colors   
    { regex: /\$<black>/, result: '\x1b[30m' },
    { regex: /\$<red>/, result: '\x1b[31m' },
    { regex: /\$<green>/, result: '\x1b[32m' },
    { regex: /\$<yellow>/, result: '\x1b[33m' },
    { regex: /\$<blue>/, result: '\x1b[34m' },
    { regex: /\$<magenta>/, result: '\x1b[35m' },
    { regex: /\$<cyan>/, result: '\x1b[36m' },
    { regex: /\$<white>/, result: '\x1b[37m' },
    { regex: /\$<gray>/, result: '\x1b[90m' },        
    { 
        regex: /\$<palette([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])>/,
        resultWithArgs: (v) => `\x1B[38;5;${v}m`
    },      
    { 
        regex: /\$<(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))>/,
        resultWithArgs: (rgb) => `\x1B[38;2;${rgb}m`
    },
    // backgrounds   
    { regex: /\$<bgBlack>/, result: '\x1b[40m' },
    { regex: /\$<bgRed>/, result: '\x1b[41m' },     
    { regex: /\$<bgGreen>/, result: '\x1b[42m' },
    { regex: /\$<bgYellow>/, result: '\x1b[43m' },        
    { regex: /\$<bgBlue>/, result: '\x1b[44m' },
    { regex: /\$<bgMagenta>/, result: '\x1b[45m' },
    { regex: /\$<bgCyan>/, result: '\x1b[46m' },
    { regex: /\$<bgWhite>/, result: '\x1b[47m' },
    { regex: /\$<bgGray>/, result: '\x1b[100m' },        
    { 
        regex: /\$<bgPalette([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])>/,
        resultWithArgs: (v) => `\x1B[48;5;${v}m`
    },     
    { 
        regex: /\$<bg(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))>/,
        resultWithArgs: (rgb) => `\x1B[48;2;${rgb}m`
    }
];

class ConsoleColor {
    private tags: Tag[] = tags;
    private regex: RegExp = this.buidReGex();
    noReset: boolean = false;

    cLog = console.log;
    log(...data: any[]): void {
        this.cLog(...data.map(msg => typeof msg === 'string' ? this.replaceTags(msg) : msg));
    }
    private buidReGex(): RegExp {
        return new RegExp(
            this.tags.reduce(
                (acc, t, i) => acc += ((i === 0 ? '' : '|') + t.regex.source), ''
            ), 'g'
        );
    }
    private replaceTags(msg: string): string {
        let matchs = 0;
        return msg.replace(
            this.regex, (match, ...groups) => {
                matchs++
                const tag = this.tags.find(t => t.regex.test(match));
                return groups && tag?.resultWithArgs
                ? tag.resultWithArgs(groups.find(p => p))
                : tag?.result ?? match
            }
        ) + (this.noReset || !matchs ? '' : '\x1b[0m');
    }
}

export const cc = new ConsoleColor();
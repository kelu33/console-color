import { cc } from './index';

describe('ConsoleColor', () => {
    const reset = '\x1b[0m';

    it('should log', () => {
        spyOn(cc, 'cLog');

        cc.log('hello test');

        expect(cc.cLog).toHaveBeenCalledWith('hello test');
    })

    it('should log with color and reset', () => {
        spyOn(cc, 'cLog');

        cc.log('$<green>hello test');

        expect(cc.cLog).toHaveBeenCalledWith('\x1b[32mhello test' + reset);
    })

    it('should log with color and NO reset', () => {
        spyOn(cc, 'cLog');
        cc.noReset = true;

        cc.log('$<green>hello test');

        expect(cc.cLog).toHaveBeenCalledWith('\x1b[32mhello test');
        cc.noReset = false;
    })

    it('should log with color palette', () => {
        spyOn(cc, 'cLog');

        cc.log('$<palette120>hello test');

        expect(cc.cLog).toHaveBeenCalledWith('\x1B[38;5;120mhello test' + reset);
    })

    it('should log with color rgb', () => {
        spyOn(cc, 'cLog');

        cc.log('$<120;120;120>hello test');

        expect(cc.cLog).toHaveBeenCalledWith('\x1B[38;2;120;120;120mhello test' + reset);
    })

    const tagsTestCases = [
        { tag: '$<reset>' , result: '\x1b[0m' },
        { tag: '$<bright>' , result: '\x1b[1m' },
        { tag: '$<dim>' , result: '\x1b[2m' },
        { tag: '$<underscore>' , result: '\x1b[4m' },
        { tag: '$<blink>' , result: '\x1b[5m' },
        { tag: '$<reverse>' , result: '\x1b[7m' },
        { tag: '$<hidden>' , result: '\x1b[8m' },

        { tag: '$<black>' , result: '\x1b[30m' },
        { tag: '$<red>' , result: '\x1b[31m' },
        { tag: '$<green>' , result: '\x1b[32m' },
        { tag: '$<yellow>' , result: '\x1b[33m' },
        { tag: '$<blue>' , result: '\x1b[34m' },
        { tag: '$<magenta>' , result: '\x1b[35m' },
        { tag: '$<cyan>' , result: '\x1b[36m' },
        { tag: '$<white>' , result: '\x1b[37m' },
        { tag: '$<gray>' , result: '\x1b[90m' },
        { tag: '$<palette33>' , result: '\x1B[38;5;33m' },
        { tag: '$<33;33;33>' , result: '\x1B[38;2;33;33;33m' },

        { tag: '$<bgBlack>' , result: '\x1b[40m' },
        { tag: '$<bgRed>' , result: '\x1b[41m' },
        { tag: '$<bgGreen>' , result: '\x1b[42m' },
        { tag: '$<bgYellow>' , result: '\x1b[43m' },
        { tag: '$<bgBlue>' , result: '\x1b[44m' },
        { tag: '$<bgMagenta>' , result: '\x1b[45m' },
        { tag: '$<bgCyan>' , result: '\x1b[46m' },
        { tag: '$<bgWhite>' , result: '\x1b[47m' },
        { tag: '$<bgGray>' , result: '\x1b[100m' },
        { tag: '$<bgPalette33>' , result: '\x1B[48;5;33m' },
        { tag: '$<bg33;33;33>' , result: '\x1B[48;2;33;33;33m' },
    ];

    tagsTestCases.forEach(test => {
        it(`should replace ${test.tag} by ${test.result}`, () => {
            spyOn(cc, 'cLog');
    
            cc.log(`Start ${test.tag} color`);
    
            expect(cc.cLog).toHaveBeenCalledWith(`Start ${test.result} color` + reset);
        })
    })
})
declare class ConsoleColor {
    private tags;
    private regex;
    noReset: boolean;
    cLog: {
        (...data: any[]): void;
        (message?: any, ...optionalParams: any[]): void;
    };
    log(...data: any[]): void;
    private buidReGex;
    private replaceTags;
}
export declare const cc: ConsoleColor;
export {};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cc = void 0;
const tags = [
    // actions
    { regex: /\$<reset>/, result: '\x1b[0m' },
    { regex: /\$<bright>/, result: '\x1b[1m' },
    { regex: /\$<dim>/, result: '\x1b[2m' },
    { regex: /\$<underscore>/, result: '\x1b[4m' },
    { regex: /\$<blink>/, result: '\x1b[5m' },
    { regex: /\$<reverse>/, result: '\x1b[7m' },
    { regex: /\$<hidden>/, result: '\x1b[8m' },
    // colors   
    { regex: /\$<black>/, result: '\x1b[30m' },
    { regex: /\$<red>/, result: '\x1b[31m' },
    { regex: /\$<green>/, result: '\x1b[32m' },
    { regex: /\$<yellow>/, result: '\x1b[33m' },
    { regex: /\$<blue>/, result: '\x1b[34m' },
    { regex: /\$<magenta>/, result: '\x1b[35m' },
    { regex: /\$<cyan>/, result: '\x1b[36m' },
    { regex: /\$<white>/, result: '\x1b[37m' },
    { regex: /\$<gray>/, result: '\x1b[90m' },
    {
        regex: /\$<palette([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])>/,
        resultWithArgs: (v) => `\x1B[38;5;${v}m`
    },
    {
        regex: /\$<(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))>/,
        resultWithArgs: (rgb) => `\x1B[38;2;${rgb}m`
    },
    // backgrounds   
    { regex: /\$<bgBlack>/, result: '\x1b[40m' },
    { regex: /\$<bgRed>/, result: '\x1b[41m' },
    { regex: /\$<bgGreen>/, result: '\x1b[42m' },
    { regex: /\$<bgYellow>/, result: '\x1b[43m' },
    { regex: /\$<bgBlue>/, result: '\x1b[44m' },
    { regex: /\$<bgMagenta>/, result: '\x1b[45m' },
    { regex: /\$<bgCyan>/, result: '\x1b[46m' },
    { regex: /\$<bgWhite>/, result: '\x1b[47m' },
    { regex: /\$<bgGray>/, result: '\x1b[100m' },
    {
        regex: /\$<bgPalette([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])>/,
        resultWithArgs: (v) => `\x1B[48;5;${v}m`
    },
    {
        regex: /\$<bg(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]);([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))>/,
        resultWithArgs: (rgb) => `\x1B[48;2;${rgb}m`
    }
];
class ConsoleColor {
    constructor() {
        this.tags = tags;
        this.regex = this.buidReGex();
        this.noReset = false;
        this.cLog = console.log;
    }
    log(...data) {
        this.cLog(...data.map(msg => typeof msg === 'string' ? this.replaceTags(msg) : msg));
    }
    buidReGex() {
        return new RegExp(this.tags.reduce((acc, t, i) => acc += ((i === 0 ? '' : '|') + t.regex.source), ''), 'g');
    }
    replaceTags(msg) {
        let matchs = 0;
        return msg.replace(this.regex, (match, ...groups) => {
            var _a;
            matchs++;
            const tag = this.tags.find(t => t.regex.test(match));
            return groups && (tag === null || tag === void 0 ? void 0 : tag.resultWithArgs)
                ? tag.resultWithArgs(groups.find(p => p))
                : (_a = tag === null || tag === void 0 ? void 0 : tag.result) !== null && _a !== void 0 ? _a : match;
        }) + (this.noReset || !matchs ? '' : '\x1b[0m');
    }
}
exports.cc = new ConsoleColor();

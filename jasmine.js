const Jasmine = require('jasmine');
const jasmine = new Jasmine();

jasmine.loadConfig({
    spec_files: ['src/**/*.spec.ts'],
    stopOnSpecFailure: false,
    stopSpecOnExpectationFailure: false,
    failSpecWithNoExpectations: false,
});
jasmine.exitOnCompletion = false;

jasmine.execute();

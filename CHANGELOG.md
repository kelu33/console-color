# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [Unreleased]

## [1.0.1] - 2023-10-18
 
### Added
- This CHANGELOG file
- TypeScript linting with esLint
- Unit tests with Jasmine
 
### Changed
 
### Fixed
- CommonJS class methodes undefined
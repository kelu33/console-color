# Console color
> Add colors in console
## Usage
```
import { cc } from '@l.baste/console-color';
// or
// const { cc } = require('@l.baste/console-color');

// Add tag to start new color, background or action
cc.log('$<green>Hello $<yellow>world $<red>color $<reset>!');

cc.log('$<palette90>hello world ! palette');
cc.log('$<0;255;255>hello world ! rgb');

cc.log('$<bgPalette90>hello world ! background palette');
cc.log('$<bg0;255;255>hello world ! background rgb');
```
## Tags
### Actions:
- $\<reset\> // Reset color and background.
- $\<bright\>
- $\<dim\>
- $\<underscore\>
- $\<blink\>
- $\<reverse\>
- $\<hidden\>
### Colors:
- $\<black\>
- $\<red\>
- $\<green\>
- $\<yellow\>
- $\<blue\>
- $\<magenta\>
- $\<cyan\>
- $\<white\>
- $\<gray\>
- $\<paletteV\> // V -> 0-255
- $\<R;G;B\> // R -> 0-255; G -> 0-255; B -> 0-255
### Backgounds
- $\<bgBlack\>
- $\<bgRed\>
- $\<bgGreen\>
- $\<bgYellow\>
- $\<bgBlue\>
- $\<bgMagenta\>
- $\<bgCyan\>
- $\<bgWhite\>
- $\<bgGray\>
- $\<bgPaletteV\> // V -> 0-255
- $\<bgR;G;B\> // R -> 0-255; G -> 0-255; B -> 0-255